import cv2
import sys
import numpy as np
import os
import json
import face_recognition

output_folder = '/home/yufebra/PycharmProjects/MyFaceRecognition/output'
dataset = '/home/yufebra/PycharmProjects/MyFaceRecognition/dataset'

print('[INFO] Processing datasets')
datasets = {}
for person_name in os.listdir(dataset):
    for filename in os.listdir(f"{dataset}/{person_name}"):
        image = face_recognition.load_image_file(f"{dataset}/{person_name}/{filename}")
        encoding = face_recognition.face_encodings(image)[0]
        image_feature = encoding.tolist()
        datasets.update({person_name : image_feature})

print('[INFO] Writing json file')
with open('dataset.json', 'w') as fp:
    json.dump(datasets, fp, indent = 4)

known_face_features = []
known_person_names = []

with open('dataset.json') as json_file:
    data = json.load(json_file)
    for ds_dict in data:
      known_face_features.append(np.array(data[ds_dict]))
      known_person_names.append(ds_dict)

# If output folder not available, create first
if not os.path.isdir(output_folder):
    print("[INFO] Output folder not available. Creating output folder first")
    os.mkdir(output_folder)

# Initialize some variables
print("[INFO] Initializing webcam to capture...")
face_locations = []
face_encodings = []
face_names = []
face_resize = ""
process_this_frame = True
video_capture = cv2.VideoCapture(0)
print("[INFO] Camera started! Press \'q\' or \'esc\' to exit. Press \'space\' to capture known face")
while True:
    ret, frame = video_capture.read()

    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
    rgb_small_frame = small_frame[:, :, ::-1]

    # Only process every other frame of video to save time
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

        face_names = []
        for face_encoding in face_encodings:
            # Find face in frame and compare it with dataset
            matches = face_recognition.compare_faces(known_face_features, face_encoding)
            name = "Unknown"

            # Find best match for getting his/her name
            face_distances = face_recognition.face_distance(known_face_features, face_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_person_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame

    # Display the results
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # Scale back up face locations since the frame we detected in was scaled to 1/4 size
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (255, 255, 0), 2)

        # Show his/her name
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 0.5, (255, 255, 0), 1)

    # Display the resulting image
    cv2.imshow('Video', frame)

    key = cv2.waitKey(10)
    # Check if user press q or esc
    if key == 27 or key == 113:
        break
    # Check if user press space
    elif key == 32:
        cv2.imwrite('% s/% s.png' % (output_folder, name), frame)
        print('Captured! Saved at % s/% s.png' % (output_folder, name))

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
print("[INFO] Program ended. Goodbye :)")