import cv2
import sys
import numpy
import os
import json
import face_recognition

haar_file = 'haarcascade_frontalface_default.xml'
outputFolder = '/home/yufebra/PycharmProjects/MyFaceRecognition/output'
dataset = '/home/yufebra/PycharmProjects/MyFaceRecognition/dataset'

print('[INFO] Processing datasets')
datasets = {}
for name in os.listdir(dataset):
    for filename in os.listdir(f"{dataset}/{name}"):
        image = face_recognition.load_image_file(f"{dataset}/{name}/{filename}")
        encoding = face_recognition.face_encodings(image)[0]
        fitur = encoding.tolist()
        datasets.update({name : fitur})

print('[INFO] Writing json file')
with open('dataset.json', 'w') as fp:
    json.dump(datasets, fp, indent = 4)

print(dat)
# If output folder not available, create first
if not os.path.isdir(outputFolder):
    os.mkdir(outputFolder)

# Image size
(width, height) = (130, 100)
face_cascade = cv2.CascadeClassifier(haar_file)

print("Starting Webcam to capture....")
webcam = cv2.VideoCapture(0)

count = 1
face_resize = ""
print("[COMMAND]\nPress q or esc to Exit\nPress space to Capture")
while True:
    (_, im) = webcam.read()
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 4)
    for (x, y, w, h) in faces:
        cv2.rectangle(im, (x, y), (x + w, y + h), (255, 0, 0), 2)
        face = gray[y:y + h, x:x + w]
        face_resize = cv2.resize(face, (width, height))
    cv2.imshow('OpenCV', im)
    key = cv2.waitKey(10)
    # Check if user press q or esc
    if key == 27 or key == 113:
        break
    # Check if user press space
    elif key == 32:
        cv2.imwrite('% s/% s.png' % (outputFolder, count), face_resize)
        print('Captured! Saved at % s/% s.png' % (outputFolder, count))
        count += 1

print("Goodbye :)")
